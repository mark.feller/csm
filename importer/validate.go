package importer

import (
	"errors"
	"fmt"

	csm "gitlab.com/t4spartners/csm-go-lib"

	"github.com/blang/semver"
	"github.com/fatih/color"
)

type warning error
type ValidationFunc func(*csm.Session, *ImportData) (error, warning)

var teamsValidation = map[string]ValidationFunc{
	"RequiredColumns": teamsRequiredColumns,
}

var usrteamsValidation = map[string]ValidationFunc{
	"RequiredColumns":    usrteamsRequiredColumns,
	"ManagerExists":      usrteamsHasManager,
	"DefaultTeamVersion": usrteamsDefaultTeamVersion,
}

// var objectValidation = []ValidationFunc{
//	objectExists,
// }

func Validate(s *csm.Session, data *ImportData, validators map[string]ValidationFunc) bool {
	pass := true
	debug("Begining Validation")
	for k, fn := range validators {
		if err, warn := fn(s, data); err != nil {
			fmt.Printf("[%s ]   %s: %v\n", color.RedString("failed"), k, err)
			pass = false
		} else if warn != nil {
			fmt.Printf("[%s]   %s: %v\n", color.YellowString("warning"), k, warn)
		} else {
			debugf("[%s ]   %s\n", color.GreenString("passed"), k)
		}
	}
	debug("Done Validation")
	return pass
}

var Verbose bool

func debugf(format string, v ...interface{}) {
	if Verbose {
		fmt.Printf(format, v...)
	}
}

func debug(v ...interface{}) {
	if Verbose {
		fmt.Println(v...)
	}
}

func teamsRequiredColumns(s *csm.Session, data *ImportData) (error, warning) {
	var teamname, teamtype bool
	for _, v := range data.Header {
		teamname = (teamname || (v == "TeamName"))
		teamtype = (teamtype || (v == "TeamType"))
	}
	if !teamname && !teamtype {
		return errors.New("missing required columns TeamName and TeamType"), nil
	} else if !teamname {
		return errors.New("missing required column TeamName"), nil
	} else if !teamtype {
		return errors.New("missing required column TeamType"), nil
	}
	return nil, nil
}

func usrteamsRequiredColumns(s *csm.Session, data *ImportData) (error, warning) {
	var teamname, fullname bool
	for _, v := range data.Header {
		teamname = (teamname || (v == "TeamName"))
		fullname = (fullname || (v == "FullName"))
	}
	if !teamname && !fullname {
		return errors.New("missing required columns TeamName and FullName"), nil
	} else if !teamname {
		return errors.New("missing required column TeamName"), nil
	} else if !fullname {
		return errors.New("missing required column FullName"), nil
	}
	return nil, nil
}

func usrteamsHasManager(s *csm.Session, data *ImportData) (error, warning) {
	for _, v := range data.Header {
		if v == "IsManager" {
			return nil, nil
		}
	}
	return nil, errors.New("missing IsManager column. defaulting to false")
}

func usrteamsDefaultTeamVersion(s *csm.Session, data *ImportData) (error, warning) {

	for _, v := range data.Header {
		if v == "DefaultTeam" {
			v, _ := s.Version()
			if minVersion(v, "9.2.0") {
				return nil, fmt.Errorf("csm version %s does not support assigning default teams", v)
			}
			break
		}
	}
	return nil, nil
}

func minVersion(cur, min string) bool {
	v1, _ := semver.Make(cur)
	v2, _ := semver.Make(min)
	return v2.GT(v1)
}
