package importer

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"

	"github.com/ivpusic/grpool"
	csm "gitlab.com/t4spartners/csm-go-lib"
	"gitlab.com/t4spartners/csm-go-lib/models"
	pb "gopkg.in/cheggaaa/pb.v1"
)

var (
	uMap map[string]string
	tMap map[string]string
	sMap map[string]string
)

type ImporterT struct {
	s        *csm.Session
	wg       *sync.WaitGroup
	bar      *pb.ProgressBar
	pool     *grpool.Pool
	validate bool
}

func NewImporterT(s *csm.Session, psize int, novalidate bool) *ImporterT {
	return &ImporterT{
		s:        s,
		wg:       &sync.WaitGroup{},
		bar:      nil,
		pool:     grpool.NewPool(psize, 100),
		validate: !novalidate,
	}
}

func (i *ImporterT) setRecordNumber(n int) {
	bar := pb.New(n)
	bar.ShowTimeLeft = false
	bar.ShowPercent = false
	bar.SetMaxWidth(72)
	bar.Start()
	i.bar = bar
	i.wg.Add(n)
}

func (i *ImporterT) done() {
	i.wg.Wait()
	i.bar.Finish()
}

type ImportData struct {
	Header  map[int]string
	Records [][]string
}

func buildUsersMap(users []map[string]string) {
	for _, u := range users {
		uMap["FullName"] = u["RecID"]
	}
}

func buildTeamMap(teams []*models.Team) {
	tMap = make(map[string]string, len(teams))
	for _, t := range teams {
		tMap[t.TeamName] = t.TeamID
	}
}

func buildSecurityMap(sec []*models.SecurityGroup) {
	sMap = make(map[string]string, len(sec))
	for _, s := range sec {
		sMap[s.GroupName] = s.GroupID
	}
}

func (i *ImporterT) ImportTeams(data *ImportData) []error {
	// run validation checks
	if i.validate {
		if passed := Validate(i.s, data, teamsValidation); !passed {
			return []error{errors.New("failed to pass validation")}
		}
	}

	i.setRecordNumber(len(data.Records) - 1)
	// validate required fields exist
	errs := make([]error, 0)
	for j, _ := range data.Records[1:] {
		row := j + 1
		i.pool.JobQueue <- func() {
			defer i.wg.Done()
			defer i.bar.Increment()

			r := data.Records[row]
			record := make(map[string]string, len(r))
			for i, v := range r {
				record[data.Header[i]] = v
			}

			if _, err := i.s.SaveTeam(record); err != nil {
				errs = append(errs, fmt.Errorf("row %d: could not save team: %v", row, err))
			}
		}
	}
	i.done()
	return errs
}

func (i *ImporterT) ImportObject(data *ImportData, object string) []error {
	errs := make([]error, 0)

	// Insert as business object
	o := i.s.O(object)

	i.setRecordNumber(len(data.Records) - 1)
	for j, _ := range data.Records[1:] {
		row := j + 1
		i.pool.JobQueue <- func() {
			defer i.wg.Done()
			defer i.bar.Increment()

			r := data.Records[row]
			record := make(map[string]string, len(r))
			for i, v := range r {
				record[data.Header[i]] = v
			}
			if _, err := o.Save(record); err != nil {
				b, _ := json.Marshal(err)
				errs = append(errs, fmt.Errorf("could not save row %d: %s\n", row+1, string(b)))
			}
		}
	}
	i.done()
	return errs
}

func (i *ImporterT) ImportUsers(data *ImportData) []error {
	sec, err := i.s.SecurityGroups()
	if err != nil {
		return []error{fmt.Errorf("could not get security groups: %v", err)}
	}
	buildSecurityMap(sec)

	i.setRecordNumber(len(data.Records) - 1)
	errs := make([]error, 0)
	for j, _ := range data.Records[1:] {
		row := j + 1
		i.pool.JobQueue <- func() {
			defer i.wg.Done()
			defer i.bar.Increment()

			r := data.Records[row]
			record := make(map[string]string, len(r))
			for i, v := range r {
				if data.Header[i] == "SecurityGroup" {
					// Get security group id
					record["SecurityGroupID"] = sMap[v]
				} else if data.Header[i] == "SecurityGroupID" {
					continue
				} else {
					record[data.Header[i]] = v
				}
			}

			if _, err := i.s.SaveUser(record); err != nil {
				errs = append(errs, fmt.Errorf("row %d: could not save user: %v", row, err))
			}
		}
	}
	i.done()
	return errs
}

func (i *ImporterT) ImportUsersTeams(data *ImportData) []error {
	// run validation checks
	if i.validate {
		if passed := Validate(i.s, data, usrteamsValidation); !passed {
			return []error{errors.New("failed to pass validation")}
		}
	}

	teams, err := i.s.Teams()
	if err != nil {
		return []error{fmt.Errorf("could not get teams: %v", err)}
	}
	buildTeamMap(teams)

	i.setRecordNumber(len(data.Records) - 1)
	errs := make([]error, 0)
	for j, _ := range data.Records[1:] {
		row := j + 1
		i.pool.JobQueue <- func() {
			defer i.wg.Done()
			defer i.bar.Increment()

			var userID, teamID string
			var isManager, isDefault bool
			r := data.Records[row]
			for k, v := range r {
				if data.Header[k] == "TeamName" {
					var ok bool
					team := strings.Trim(v, " ") // cleanup trailing whitespace
					if teamID, ok = tMap[team]; !ok {
						errs = append(errs, fmt.Errorf("row %d: could not find team \"%s\"\n", row, team))
						return
					}
				} else if data.Header[k] == "FullName" {
					name := strings.Trim(v, " ") // cleanup trailing whitespace
					if id, err := i.s.User(name); err != nil {
						errs = append(errs, fmt.Errorf("row %d: could not find user \"%s\"\n", row, name))
						return
					} else {
						userID = id
					}
				} else if data.Header[k] == "IsManager" {
					isManager = (v == "true")
				} else if data.Header[k] == "DefaultTeam" {
					isDefault = (v == "true")
				}
			}

			if err := i.s.AddUserToTeam(userID, teamID, isManager, isDefault); err != nil {
				b, _ := json.Marshal(err)
				errs = append(errs, fmt.Errorf("row %d: could not add user to team (%s, %s, %v): %s\n", row, userID, teamID, isManager, string(b)))
			}
		}
	}
	i.done()
	return errs
}
