# Uses multistage build in docker 17.05

# Build Container
FROM golang:alpine
RUN apk -U add make git
WORKDIR /go/src/gitlab.com/t4spartners/csm
ADD . .
RUN make deps; make install

# Final Build Image
FROM alpine:latest

RUN apk -U add ca-certificates
COPY --from=0 /go/bin/csm /bin
ENTRYPOINT ["/bin/csm"]
