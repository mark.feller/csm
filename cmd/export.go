// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// exportCmd represents the export command
var exportCmd = &cobra.Command{
	Use:   "export",
	Short: "export business object data to csv",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			fmt.Fprintln(os.Stderr, "expected at least one argument")
			return
		}

		// output
		var w io.Writer
		var err error
		if out := viper.GetString("outfile"); out == "" {
			w = os.Stdout
		} else {
			w, err = os.OpenFile(out, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
			if err != nil {
				fmt.Fprintln(os.Stderr, "export failed:", err)
			}
		}

		s := openSession()

		var results []map[string]interface{}
		if err := s.O(args[0]).Find().All(&results); err != nil {
			fmt.Fprintln(os.Stderr, "could not retieve data:", err)
			return
		}

		records, err := buildCSV(results)
		if err != nil {
			fmt.Fprintln(os.Stderr, "could not build csv:", err)
			return
		}

		c := csv.NewWriter(w)
		if err := c.WriteAll(records); err != nil {
			fmt.Fprintln(os.Stderr, "could not write csv:", err)
		}
	},
}

func init() {
	RootCmd.AddCommand(exportCmd)

	exportCmd.Flags().StringP("outfile", "o", "", "export to a file")
	viper.BindPFlag("outfile", exportCmd.Flags().Lookup("outfile"))
}

func buildCSV(results []map[string]interface{}) ([][]string, error) {
	if len(results) == 0 {
		return nil, errors.New("result length 0")
	}

	var (
		wg        = sync.WaitGroup{}
		records   = make([][]string, len(results)+1)
		size      = len(results[0])
		header    = make([]string, size)
		headerMap = make(map[string]int, size)
		i         = 0
	)

	for k, _ := range results[0] {
		header = append(header, k)
		headerMap[k] = i
		i++
	}
	records[0] = header

	wg.Add(len(results))
	for index, res := range results {
		go func(i int, r map[string]interface{}) {
			defer wg.Done()

			record := make([]string, size)
			for k, v := range r {
				record[headerMap[k]] = v.(string)
			}
			records[i] = record
		}(index+1, res)
	}

	wg.Wait()
	return records, nil
}
