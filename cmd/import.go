// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"encoding/csv"
	"errors"
	"fmt"
	"os"

	"gitlab.com/t4spartners/csm/importer"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:    "import [flags] [object]",
	Short:  "import data from a csv",
	PreRun: addImportConfig,
	Run: func(cmd *cobra.Command, args []string) {
		file := viper.GetString("infile")
		i := importer.NewImporterT(openSession(), viper.GetInt("poolsize"), viper.GetBool("no-validation"))
		importer.Verbose = viper.GetBool("verbose")
		data, err := readImportFile(file)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			return
		}

		var errs []error
		if viper.GetBool("users") {
			errs = i.ImportUsers(data)
		} else if viper.GetBool("teams") {
			errs = i.ImportTeams(data)
		} else if viper.GetBool("usersteams") {
			errs = i.ImportUsersTeams(data)
		} else {
			errs = i.ImportObject(data, viper.GetString("object"))
		}

		// log errors
		for _, err := range errs {
			fmt.Fprintln(os.Stderr, err)
		}
	},
	SilenceUsage: true,
}

func init() {
	RootCmd.AddCommand(importCmd)

	importCmd.Flags().StringP("file", "f", "", "csv file to import from")
	viper.BindPFlag("infile", importCmd.Flags().Lookup("file"))

	importCmd.Flags().StringP("import-config", "i", "", "import config file")
	viper.BindPFlag("import-config", importCmd.Flags().Lookup("import-config"))

	importCmd.Flags().StringP("object", "o", "", "import object name")
	viper.BindPFlag("object", importCmd.Flags().Lookup("object"))

	importCmd.Flags().Bool("teams", false, "import teams")
	viper.BindPFlag("teams", importCmd.Flags().Lookup("teams"))

	importCmd.Flags().Bool("users", false, "import users")
	viper.BindPFlag("users", importCmd.Flags().Lookup("users"))

	importCmd.Flags().Bool("usersteams", false, "import user team association")
	viper.BindPFlag("usersteams", importCmd.Flags().Lookup("usersteams"))

	importCmd.Flags().Int("poolsize", 500, "network request pool size")
	viper.BindPFlag("poolsize", importCmd.Flags().Lookup("poolsize"))

	importCmd.Flags().Bool("no-validation", false, "disable preimport validation")
	viper.BindPFlag("no-validation", importCmd.Flags().Lookup("no-validation"))
}

// add import config file to viper
func addImportConfig(cmd *cobra.Command, args []string) {
	if f := viper.GetString("import-config"); f != "" {
		fd, err := os.Open(f)
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not read import config file %s: %s\n", f, err)
			return
		}
		defer fd.Close()
		viper.MergeConfig(fd)
	}
}

func readImportFile(file string) (*importer.ImportData, error) {
	if file == "" {
		return nil, errors.New("no import file specified. Use the -f option to specify a file")
	}
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("could not read file %s: %v", file, err)
	}
	r, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return nil, fmt.Errorf("could not parse csv file %s: %v", file, err)
	}
	if len(r)-1 < 1 {
		return nil, errors.New("nothing to save")
	}
	h := make(map[int]string, len(r[0]))
	for i, v := range r[0] {
		h[i] = v
	}
	return &importer.ImportData{Header: h, Records: r}, nil
}
