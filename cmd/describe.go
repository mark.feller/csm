// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	csm "gitlab.com/t4spartners/csm-go-lib"
	"gitlab.com/t4spartners/csm-go-lib/models"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// describeCmd represents the describe command
var describeCmd = &cobra.Command{
	Use:   "describe",
	Short: "display information about business objects",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			fmt.Println("expected at least one argument")
			return
		}

		s := openSession()

		if viper.GetBool("describe.full") {
			if schema, err := s.O(args[0]).Schema(); err != nil {
				fmt.Fprintln(os.Stderr, "could not get object schema:", err)
			} else {
				if err := json.NewEncoder(os.Stdout).Encode(schema); err != nil {
					log.Fatal(err)
				}
			}
			return
		}

		if viper.GetBool("describe.relations") {
			if schema, err := s.O(args[0]).Schema(); err != nil {
				fmt.Fprintln(os.Stderr, "could not get object schema:", err)
			} else {
				for _, sch := range schema.Relationships {
					fmt.Printf("%s: %s\n", sch.DisplayName, sch.RelationshipID)
				}
			}
			return
		}

		o, err := loadObjectInfo(args[0], s)
		if err != nil {
			fmt.Fprintln(os.Stderr, "could not get object info:", err)
			return
		}

		if viper.GetBool("describe.json") {
			e := json.NewEncoder(os.Stdout)
			if err := e.Encode(o); err != nil {
				log.Fatal(err)
			}
		} else {
			fmt.Println(o.Stringer())
		}
	},
}

func init() {
	RootCmd.AddCommand(describeCmd)

	describeCmd.Flags().Bool("json", false, "format output as json")
	viper.BindPFlag("describe.json", describeCmd.Flags().Lookup("json"))

	describeCmd.Flags().Bool("full", false, "complete schema response")
	viper.BindPFlag("describe.full", describeCmd.Flags().Lookup("full"))

	describeCmd.Flags().Bool("relations", false, "print relationships")
	viper.BindPFlag("describe.relations", describeCmd.Flags().Lookup("relations"))
}

func objectType(sum *models.Summary) string {
	if sum.Major {
		return "Major"
	} else if sum.Supporting {
		return "Supporting"
	} else if sum.Lookup {
		return "Lookup"
	} else {
		return "Minor"
	}
}

type objectInfo struct {
	DisplayName string
	Name        string
	BusObID     string
	Type        string
	RecIDField  string
	Fields      []fieldInfo
}

type fieldInfo struct {
	Name string
	ID   string
}

func loadObjectInfo(object string, s *csm.Session) (*objectInfo, error) {
	sum, err := s.O(object).Summary()
	if err != nil {
		return nil, err
	}
	schema, err := s.O(object).Schema()
	if err != nil {
		return nil, err
	}

	o := &objectInfo{
		DisplayName: sum.DisplayName,
		Name:        sum.Name,
		BusObID:     sum.BusObID,
		Type:        objectType(sum),
		RecIDField:  sum.RecIDFields,
	}

	for _, f := range schema.FieldDefinitions {
		field := fieldInfo{
			Name: f.Name,
			ID:   f.FieldID,
		}
		o.Fields = append(o.Fields, field)
	}

	return o, nil
}

func (o *objectInfo) Stringer() string {
	s := fmt.Sprintf(`Name: %s
Display Name: %s
Name: %s
BusObID: %s
Type: %s
RecID Field: %s
Fields:`, o.DisplayName, o.Name, o.DisplayName, o.BusObID, o.Type, o.RecIDField)

	for _, f := range o.Fields {
		s = s + fmt.Sprintf(`
 Name: %s`, f.Name)
	}

	return s
}
