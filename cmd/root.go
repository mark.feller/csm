// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"
	"strings"

	csm "gitlab.com/t4spartners/csm-go-lib"
	"gitlab.com/t4spartners/csm-go-lib/auth"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string
var version string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "csm",
	Short: "Cherwell Service Management helper commands",
	Long:  `A suite of cherwell service manager helper functions including import, export, and swagger generation`,
	Run: func(cmd *cobra.Command, args []string) {
		if viper.GetBool("version") {
			fmt.Printf("Cherwell Service Management CLI %s\n", version)
		} else {
			cmd.Usage()
		}
	},
	SilenceUsage: true,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")

	RootCmd.PersistentFlags().BoolP("verbose", "v", false, "verbose output")
	viper.BindPFlag("verbose", RootCmd.PersistentFlags().Lookup("verbose"))

	RootCmd.Flags().Bool("version", false, "print version")
	viper.BindPFlag("version", RootCmd.Flags().Lookup("version"))

	RootCmd.PersistentFlags().Bool("vv", false, "print every network request")
	viper.BindPFlag("vv", RootCmd.PersistentFlags().Lookup("vv"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetEnvPrefix("csm")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName(".cherwell")
	viper.AddConfigPath("$HOME")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err == nil {
		if viper.GetBool("verbose") {
			fmt.Println("Using config file:", viper.ConfigFileUsed())
		}
	} else {
		if viper.GetBool("verbose") {
			fmt.Fprintln(os.Stderr, "err during read config:", err)
		}
	}
}

func openSession() (s *csm.Session) {
	var (
		url    = viper.GetString("cherwell.url")
		apikey = viper.GetString("cherwell.apikey")
		user   = viper.GetString("cherwell.user")
		pass   = viper.GetString("cherwell.pass")
		ssl    = viper.GetBool("cherwell.ssl")
	)
	if ssl {
		a := auth.NewAuthSSL(url, apikey, &user, &pass)
		s = csm.NewSessionSSL(url).WithAuth(a.BearerTokenF)
	} else {
		a := auth.NewAuth(url, apikey, &user, &pass)
		s = csm.NewSession(url).WithAuth(a.BearerTokenF)
	}
	if viper.GetBool("vv") {
		s.WithDebug()
	}
	return s
}
