// Copyright © 2017 Mark Feller <mjfeller1992@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/t4spartners/csm/spec"
)

// specCmd represents the spec command
var specCmd = &cobra.Command{
	Use:   "spec",
	Short: "Generate a worp swagger spec for a business object",
	Run: func(cmd *cobra.Command, args []string) {
		s := openSession()
		c := &spec.Config{
			Server:  viper.GetString("cherwell.url"),
			Objects: viper.GetStringSlice("objects"),
			Title:   viper.GetString("title"),
			URL:     viper.GetString("url"),
		}

		sp := spec.New(c).WithSession(s)
		sp.InitDefinitions()
		b, err := sp.Swagger.MarshalJSON()
		if err != nil {
			fmt.Fprintln(os.Stderr, "could not marshal spec:", err)
		} else {
			fmt.Println(string(b))
		}
	},
}

func init() {
	RootCmd.AddCommand(specCmd)

	specCmd.Flags().StringSliceP("objects", "o", []string{}, "objects to have spec generated for")
	viper.BindPFlag("objects", specCmd.Flags().Lookup("objects"))

	specCmd.Flags().StringP("title", "t", "worp", "server title")
	viper.BindPFlag("title", specCmd.Flags().Lookup("title"))

	specCmd.Flags().String("url", "", "url of the cherwell instance")
	viper.BindPFlag("url", specCmd.Flags().Lookup("url"))
}
