// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"
	"sync"

	"github.com/ivpusic/grpool"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	pb "gopkg.in/cheggaaa/pb.v1"
)

// deleteCmd represents the delete command
var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "delete all instances of a business object",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			fmt.Fprintln(os.Stderr, "extected at least one argument")
			return
		}

		var (
			s       = openSession()
			wg      = sync.WaitGroup{}
			pool    = grpool.NewPool(viper.GetInt("poolsize"), 100)
			results []map[string]string
		)

		if err := s.O(args[0]).Find().Project("RecID").All(&results); err != nil {
			fmt.Fprintln(os.Stderr, "could not search business object:", err)
			return
		}

		if len(results) == 0 {
			fmt.Println("nothing to delete")
			return
		}

		// setup progress bar and wait group
		wg.Add(len(results))
		bar := pb.New(len(results))
		bar.ShowTimeLeft = false
		bar.ShowPercent = false
		bar.SetMaxWidth(72)
		bar.Start()

		for _, res := range results {
			r := res
			pool.JobQueue <- func() {
				defer wg.Done()
				defer bar.Increment()

				if err := s.O(args[0]).Delete(r["RecID"]); err != nil {
					fmt.Fprintf(os.Stderr, "could not delete object %s: %s\n", r["RecID"], err)
				}
			}
		}
		wg.Wait()
		bar.Finish()
	},
}

func init() {
	RootCmd.AddCommand(deleteCmd)

	deleteCmd.Flags().BoolP("yes", "y", false, "skip questions, used for scripts")
	viper.BindPFlag("yes", deleteCmd.Flags().Lookup("yes"))

	deleteCmd.Flags().Bool("ssl", false, "use ssl")
	viper.BindPFlag("cherwell.ssl", deleteCmd.Flags().Lookup("ssl"))

	deleteCmd.Flags().Int("poolsize", 100, "concurrent delete request to cherwell")
	viper.BindPFlag("poolsize", deleteCmd.Flags().Lookup("poolsize"))
}
